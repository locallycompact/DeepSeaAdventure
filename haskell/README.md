# Deep Sea Adventure (TUI Edition)

A TUI version of the board game [Deep Sea Adventure](https://oinkgms.com/en/deep-sea-adventure) written in [brick](http://hackage.haskell.org/package/brick). Not for commercial use.
