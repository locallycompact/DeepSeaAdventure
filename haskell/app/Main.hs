{-# LANGUAGE NoMonomorphismRestriction, TemplateHaskell #-}
module Main where

import           Brick
import           Control.Lens
import           Control.Monad.Random.Class
import           DSA.Display
import           DSA.Types
import           RIO hiding (Lens', lens, view, (^.))
import           RIO.Partial
import           RIO.List.Partial
import qualified RIO.Map as Map
import           RIO.State
import           System.IO
import           System.Random.Shuffle

deriving instance MonadRandom (RIO env)

---

newBoard :: MonadRandom m => [Player] -> m Board
newBoard xs = do
  chips <- join <$> forM [1,2,3,4] (shuffleM . starsToChips)
  return $ Board 25 (Trail $ Submarine : (SingleTreasure <$> chips))
    (Map.fromList $ (,PlayerData 0 Descending [] []) <$> xs)

calculateNewPosition :: Trail -> Map Player Int -> Player -> Dir -> Int -> Int
calculateNewPosition (Trail t) m p d n = 
  case d of
    Descending -> min deepestFreeSpace descent
    Ascending   -> max 0 ascent
  where
    deepestFreeSpace = let f x = if x `elem` Map.elems m then f (x - 1) else x in f (length t - 1)
    descent = let f x n 
                    | n == 0 = x
                    | (x + 1) `elem` Map.elems m = f (x + 1) n
                    | otherwise = f (x + 1) (n - 1)
              in f c n
    ascent = undefined
    c = fromJust $ Map.lookup p m

movePlayer :: MonadState Board m => Player -> Int -> m Tile
movePlayer p n = get >>= \Board{..} -> do
  let x = calculateNewPosition _trail (view pos <$> _playerData) p (view dir . fromJust . Map.lookup p $ _playerData) n
  playerData . ix p . pos <.= x
  (!! x) . unTrail <$> use trail

takeTreasure :: MonadState Board m => Player -> m [Chip]
takeTreasure = undefined

sinkPlayer :: MonadState Board m => Player -> m ()
sinkPlayer = undefined

contractTrail :: Trail -> Trail
contractTrail (Trail xs) = Trail (filter (/= Ruin) xs)

---

starsToChips :: Int -> [Chip]
starsToChips = join . replicate 2 . (<$> [0, 1, 2, 3]) . liftA2 (.) Chip ((+) . subtract 1)

----

newtype Game state env a = Game { unGame :: StateT state (RIO env) a }
  deriving (Functor, Applicative, Monad, MonadIO, MonadReader env, MonadState state)

runGame :: MonadIO m => state -> env -> Game state env a -> m (a, state)
runGame state env (Game (StateT f)) = runRIO env (f state)

renderGame :: (Display state, HasLogFunc env) => Game state env ()
renderGame = logInfo . display =<< get

----

main :: IO ()
main = do
  hSetEncoding stdout utf8
  hSetEncoding stderr utf8
  void $ runSimpleApp $ do
    x <- newBoard [Purple, Green, Orange]
    liftIO $ defaultMain ((simpleApp (mockLayout x)) { appAttrMap = attrs }) x
