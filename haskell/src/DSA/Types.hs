{-# language TemplateHaskell #-}

module DSA.Types where

import Control.Lens
import RIO hiding (lens, Lens')

data Dir = Ascending | Descending
  deriving (Eq, Show)

data Chip = Chip {
  _stars :: Int
, _value :: Int
} deriving (Eq, Show, Ord)

$(makeLenses ''Chip)

instance Display Chip where           
  display (Chip 1 _) = display ("[△]" :: Text)
  display (Chip 2 _) = display ("[◇]" :: Text)
  display (Chip 3 _) = display ("[⬠]" :: Text)
  display (Chip 4 _) = display ("[⬡]" :: Text)

data Player = Purple | Green | Yellow | Red | Blue | Orange       
  deriving (Eq, Show, Enum, Ord, Bounded)

data Tile = Submarine | SingleTreasure Chip | TreasureStack [Chip] | Ruin
  deriving (Eq, Show)

newtype Trail = Trail { unTrail :: [Tile] }
  deriving (Eq, Show)

data PlayerData = PlayerData {
  _pos   :: Int
, _dir   :: Dir
, _haul  :: [Chip]
, _stash :: [Chip]
} deriving (Eq, Show)

$(makeLenses ''PlayerData)

data Board = Board {
  _air        :: Int
, _trail      :: Trail
, _playerData :: Map Player PlayerData
} deriving (Eq, Show)

$(makeLenses ''Board)

data Action = Roll | TurnAround | Pickup
  deriving (Eq, Ord, Show)

newtype Ext = ActionX Action
  deriving (Eq, Ord, Show)

