{-# language TemplateHaskell #-}

module DSA.Display where

import           Brick
import           Brick.Widgets.Border
import           Brick.Widgets.Border.Style
import           Data.FileEmbed
import           DSA.Types
import           Graphics.Vty
import           RIO
import           RIO.List
import qualified RIO.Map as Map
import qualified RIO.Text as T

--- Resources ---

subTopAscii :: Text
subTopAscii = T.decodeUtf8With T.lenientDecode $(embedFile "art/subtop.txt")

subBotAscii :: Text
subBotAscii = T.decodeUtf8With T.lenientDecode $(embedFile "art/subbot.txt")

--- Players ---

playerWidget :: Player -> Widget n
playerWidget x = withAttr (fromString $ show x) $ txt "O"

playerAttrs :: [(AttrName, Attr)]
playerAttrs = [
  ("Purple", fg magenta)
 ,("Green", fg green)
 ,("Blue", fg blue)
 ,("Orange", fg $ rgbColor 255 165 0)
 ,("Red", fg red)
 ,("Yellow", fg yellow)
 ]

--- Submarine ---

subCabin :: [Player] -> Widget n
subCabin xs = txt "  .    |      " <+> peeps <+> txt "      |"
  where peeps = hBox $ fmap (\x -> if x `elem` xs then playerWidget x else str " ") [Purple ..]

subWidget :: [Player] -> Widget n
subWidget x = txt subTopAscii <=> subCabin x <=> txt subBotAscii

--- HUD ---

airWidget :: Int -> Widget n      
airWidget x = withBorderStyle ascii $ border $ str "Air: " <+> str (show x)
 
scoreRowItem :: Player -> [Chip] -> Widget n
scoreRowItem p xs = withAttr (fromString . show $ p) $
  str (show p) <+> txt ":" <+> pad p <+> str (show $ score xs)
  where pad x = str $ replicate (8 - length (show x)) ' '

score :: [Chip] -> Int -- TODO: Move this
score xs = sum (map _value xs)

scoreBoard :: Map Player [Chip] -> Widget n
scoreBoard xs = borderWithLabel (str "ScoreBoard") $ vBox $ map (uncurry scoreRowItem) leaders
  where leaders = sortOn (score . snd) (Map.toList xs)

mkButton :: Action -> Widget Ext
mkButton action = reportExtent (ActionX action)
                $ padBottom (Pad 1)
                $ str "[" <+>
                withAttr (attrName "btnAttr") (str (show action)) <+> str "]"

actionBoard :: Widget Ext
actionBoard = border $ mkButton Roll <=> mkButton TurnAround <=> mkButton Pickup

statusBoard :: Board -> Widget n
statusBoard board = airWidget (_air board) <=> scoreBoard (_haul <$> _playerData board)

--- Game Board ---

gameZone :: Board -> Widget n
gameZone board = withBorderStyle ascii $ border $ subWidget subPlayers
  where subPlayers = map fst . filter (\x -> snd x == 0) $ Map.toList (_pos <$> _playerData board)

--- Main ---

attrs :: b -> AttrMap
attrs = const $ attrMap defAttr playerAttrs

mockLayout :: Board -> Widget Ext
mockLayout board = gameZone board <+> (statusBoard board <=> actionBoard)
