module Main

import Control.ST
import Data.List
import Data.SortedMap
import Data.Vect
import Data.HVect

data Chip : Type where
  MkChip : ( stars : Int ) -> ( value : Int ) -> Chip

data Tile : Type where
  Submarine : Tile
  SingleTreasure : Chip -> Tile
  TreasureStack : { n : Fin 4 } -> Vect (finToNat n) Chip -> Tile
  Ruin : Tile

data Player : Type where
  Blue   : Player
  Green  : Player
  Orange : Player
  Purple : Player
  Red    : Player
  Yellow : Player

implementation Eq Player where
  Blue   == Blue   = True
  Green  == Green  = True
  Orange == Orange = True
  Purple == Purple = True
  Red    == Red    = True
  Yellow == Yellow = True
  _ == _ = False

data PlayerData : Type where
  MkPlayerData : ( haul : List Chip ) -> ( stash : List Chip ) -> PlayerData
 
stash : PlayerData -> PlayerData
stash (MkPlayerData haul stash) = MkPlayerData [] (haul ++ stash)

newBoard : ST m Var [add (State (Vect 33 Tile))]
newBoard = new (Submarine :: replicate 32 Ruin)

takeUpto : (x : Nat) -> Vect n a -> Vect (minimum x n) a
takeUpto Z ys {n} = []
takeUpto x [] = rewrite minimumZeroZeroLeft x in []
takeUpto (S x) (y::ys) = y :: takeUpto x ys

dropUpto : (x : Nat) -> Vect n a -> Vect (n `minus` minimum x n) a
dropUpto Z ys {n} = rewrite minusZeroRight n in ys
dropUpto x [] = []
dropUpto (S x) (y :: ys) = dropUpto x ys

modNatZeroNZZero : (d : Nat ) -> (prf : Not ( d = 0)) -> Z = modNatNZ 0 d prf
modNatZeroNZZero (S x) p = Refl

{-
lteFalseLteSuccNFalse : ( x, y : Nat ) -> (p : lte x y = False) -> lte (S x) y = False
lteFalseLteSuccNFalse (S x) Z p = Refl
lteFalseLteSuccNFalse (S x) (S y) p = Refl
--}


lteSuccNNFalse : ( x : Nat ) -> lte (S x) x = False
lteSuccNNFalse Z = Refl
lteSuccNNFalse (S x) = lteSuccNNFalse x

lteSuccSuccNNFalse : (x : Nat) -> lte (S (S x)) x = False
lteSuccSuccNNFalse Z = Refl
lteSuccSuccNNFalse (S x) = lteSuccSuccNNFalse x

lteFalseLteSuccFalse : (x, y : Nat) -> lte y x = False -> lte (S y) x = False
lteFalseLteSuccFalse Z Z p = absurd p
lteFalseLteSuccFalse (S x) Z p = absurd p
lteFalseLteSuccFalse (S k) (S j) p = lteFalseLteSuccFalse k j p

lteFalseLtePlusLeftFalse : (x , y, k : Nat) -> lte y x = False -> lte (y + k) x = lte y x
lteFalseLtePlusLeftFalse Z (S b) j prf = Refl
lteFalseLtePlusLeftFalse (S a) (S b) (S j) prf = lteFalseLtePlusLeftFalse a b (S j) prf

lteSelfPlusNZLeftFalse : (x, k : Nat) -> lte (x + k) x = False
lteSelfPlusNZLeftFalse Z (S j) = Refl
lteSelfPlusNZLeftFalse (S a) (S j) = lteSelfPlusNZLeftFalse a (S j)

modNatNZSelfIsZ : (d : Nat) -> ( p : Not ( d = 0)) -> Z = modNatNZ d d p
modNatNZSelfIsZ (S Z) _ = Refl
modNatNZSelfIsZ (S (S x)) _ =
  rewrite sym $ minusZeroN x in 
   rewrite lteSuccNNFalse x in Refl
 
divCeilZeroNZZero : (d : Nat) -> (p : Not (d = 0)) -> Z = divCeilNZ 0 d prf
divCeilZeroNZZero (S x) p = Refl

divNatZeroNZZero : (d : Nat) -> ( p : Not (d = 0) ) -> Z = divNatNZ 0 d prf
divNatZeroNZZero (S x) _ = Refl

divNatNZSelfIsSZ : (n : Nat) -> ( p : Not (n = 0) ) -> S Z = divNatNZ n n p
divNatNZSelfIsSZ (S Z) _ = Refl
divNatNZSelfIsSZ (S (S x)) _ =
  rewrite sym $ minusZeroN x in
    rewrite lteSuccNNFalse x in Refl

divNatNZSucc : (n : Nat) -> (d : Nat) -> (prf : Not (d = 0)) -> divNatNZ (plus n d) d prf = S (divNatNZ n d prf) 
divNatNZSucc Z (S x) p = rewrite divNatNZSelfIsSZ (S x) p in Refl
divNatNZSucc (S y) (S x) p = rewrite plusCommutative y (S x) in
                               rewrite plusSuccRightSucc x y in
                                      rewrite plusSuccRightSucc x (S y) in
                                              rewrite lteSelfPlusNZLeftFalse x (S (S y)) in ?q
                           

divCeilNZSuccMinusMinimum : (n : Nat) -> (d : Nat) -> (p : Not (n = 0) ) -> (q : Not (d = 0)) -> S (divCeilNZ n d q) = divCeilNZ (n + d) d q
divCeilNZSuccMinusMinimum (S Z) (S Z) p q = Refl
--divCeilNZSuccMinusMinimum (S (S y)) (S Z) p q = Refl

main : IO ()
main = ?p
{--
chunkIndices : (n : Nat ) -> (d : Nat) -> (prf : Not (d = 0)) -> Vect (divCeilNZ n d prf) Nat
chunkIndices Z (S x) p = [] 
chunkIndices n d p = rewrite sym $ divCeilNZSuccMinusMinimum n d p in 
                             minimum d n :: chunkIndices (n `minus` minimum d n) d p

choppedVector : (x : Nat) -> (prf : Not (x = 0)) -> Vect n a -> HVect (Vect (divCeilNZ n x prf) Type)
choppedVector x prf [] {n} = []
choppedVector x prf (y::ys) {n} = [Vect (takeUpto x xmap (\z => Vect z a) $ chunkIndices n x prf)]

f : (x : Nat) -> (p : Not (x = 0)) -> Vect n a -> Vect (minimum x n) a :: choppedVector x p (dropUpto x ys) = choppedVector x p ys
f (S x) p [] = Refl
f (S x) p (x :: xs) = Refl

break : (x : Nat) -> (prf : Not (x = 0) ) -> ( ys : Vect n a ) ->
        choppedVector x prf ys
break x p ys = takeUpto x ys :: break x p (dropUpto x ys)
--}
